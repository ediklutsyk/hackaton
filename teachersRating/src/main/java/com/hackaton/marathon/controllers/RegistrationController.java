package com.hackaton.marathon.controllers;

import com.hackaton.marathon.services.StudentService;
import com.hackaton.marathon.db.persistence.Student;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.util.Optional;

@Controller
@RequestMapping(value = "/registration")
public class RegistrationController {
    private final StudentService studentService;

    public RegistrationController(StudentService studentService) {
        this.studentService = studentService;
    }


    @GetMapping
    public String registration(Model model){
        model.addAttribute("student", new Student());
        return "registration";
    }

    @PostMapping
    public String createNewStudent(@Valid Student student, BindingResult bindingResult, Model model) {
        String email = student.getEmail();
        Optional<Student> existStudent = studentService.findBy(email);
        if (existStudent.isPresent()) {
            bindingResult.rejectValue("email", "error.student",
                    "There is already a user registered with the email provided");
        }
        if (!student.getPassword().equals(student.getConfirmPassword())){
            bindingResult.rejectValue("password","error.student", "Passwords should be equal");
        }
        if (bindingResult.hasErrors()) {
            return "registration";
        } else {
            studentService.save(student);
            model.addAttribute("successMessage", "Student has been registered successfully");
            model.addAttribute("student", new Student());
        }
        return "registration";
    }
}

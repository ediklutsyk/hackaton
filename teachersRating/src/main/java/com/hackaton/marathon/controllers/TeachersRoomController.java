package com.hackaton.marathon.controllers;

import com.hackaton.marathon.services.StudentService;
import com.hackaton.marathon.services.TeacherService;
import com.hackaton.marathon.db.persistence.Comment;
import com.hackaton.marathon.db.persistence.Student;
import com.hackaton.marathon.db.persistence.Teacher;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Controller

public class TeachersRoomController {
    private final TeacherService teacherService;
    private final StudentService studentService;

    public TeachersRoomController(TeacherService teacherService, StudentService studentService) {
        this.teacherService = teacherService;
        this.studentService = studentService;
    }

    @RequestMapping(value = "/teachers_room")
    public String teachersRoom(Model model, @RequestParam Integer id,HttpServletRequest request) {
        String studentUsername = request.getUserPrincipal().getName();
        Student student = studentService.findBy(studentUsername).get();
        Teacher teacher = teacherService.findById(id).get();
        model.addAttribute("teacher", teacher);
        double average = teacher.getComments().stream().mapToDouble(Comment::getRate).average().orElse(Double.NaN);
        double averageDifficulty = teacher.getComments().stream().mapToDouble(Comment::getDifficult).average().orElse(Double.NaN);
        List<String> tags = teacher.getComments().stream().map(Comment::getTags).flatMap(List::stream).collect(Collectors.toList());
        Map<String, Long> countedTags = tags.stream()
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        boolean hasCommented = teacher.getComments().stream().map(Comment::getStudent).anyMatch(s->s.getEmail().equals(studentUsername));
        model.addAttribute("average", String.format("%(.2f", average));
        model.addAttribute("averageDifficulty", String.format("%(.2f", averageDifficulty));
        model.addAttribute("comments", teacher.getComments());
        model.addAttribute("countedTags", countedTags);
        model.addAttribute("hasntCommented", hasCommented);
        model.addAttribute("comment",new Comment());
        return "teachers_room";
    }

    @PostMapping(value = "/comment")
    public String teachersRoom(Model model, @Valid Comment comment, @RequestParam Integer id, @RequestParam String studentUsername) {
        Teacher teacher =  teacherService.findById(id).get();
        Student student = studentService.findBy(studentUsername).get();
        teacherService.addComment(comment,teacher,student);
        model.addAttribute("teacher",teacher);
        model.addAttribute("comments", teacher.getComments());
        model.addAttribute("comment",new Comment());
        return "redirect:/teachers_room?id="+id;
    }


}

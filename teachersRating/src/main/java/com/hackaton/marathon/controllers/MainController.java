package com.hackaton.marathon.controllers;


import com.hackaton.marathon.db.persistence.School;
import com.hackaton.marathon.db.persistence.Teacher;
import com.hackaton.marathon.services.SchoolService;
import com.hackaton.marathon.services.StudentService;
import com.hackaton.marathon.services.TeacherService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Controller
public class MainController {
    private final StudentService studentService;
    private final SchoolService schoolService;
    private final TeacherService teacherService;

    public MainController(StudentService studentService, SchoolService schoolService, TeacherService teacherService) {
        this.studentService = studentService;
        this.schoolService = schoolService;
        this.teacherService = teacherService;
    }

    @RequestMapping(value = "/schools")
    public String schools(Model model) {
        model.addAttribute("schools",schoolService.findAll());
        return "schools";
    }
    @RequestMapping(value = "/school")
    public String school(@RequestParam String schoolName, Model model) {
        School school = schoolService.findByName(schoolName).get();
        Set<String> subjects = school.getTeachers().stream().map(Teacher::getSubject).collect(Collectors.toSet());
        model.addAttribute("subjects",subjects);
        model.addAttribute("school",school);
        return "subjects";
    }

    @RequestMapping(value = "/teachers")
    public String teachers(Model model, @RequestParam String subject, @RequestParam String schoolName) {
        School school = schoolService.findByName(schoolName).get();
        List<Teacher> teachers = teacherService.findAllBySubject(subject)
                .stream().filter(teacher -> teacher.getSchool().equals(school)).collect(Collectors.toList());
        model.addAttribute("teachers", teachers);
        return "teachers";
    }

    @RequestMapping(value = "/login")
    public String login() {
        return "login";
    }


}
package com.hackaton.marathon.controllers;


import com.hackaton.marathon.db.persistence.School;
import com.hackaton.marathon.db.persistence.Teacher;
import com.hackaton.marathon.services.SchoolService;
import com.hackaton.marathon.services.TeacherService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.Optional;

@Controller
@RequestMapping(value = "/admin")
public class AdminController {
    private final SchoolService schoolService;
    private final TeacherService teacherService;

    public AdminController(SchoolService schoolService, TeacherService teacherService) {
        this.schoolService = schoolService;
        this.teacherService = teacherService;
    }

    @GetMapping
    public String admin(Model model) {
        model.addAttribute("school", new School());
        model.addAttribute("teacher", new Teacher());
        model.addAttribute("schools",schoolService.findAll());
        return "admin";
    }

    @PostMapping
    public String createNewSchool(@Valid School school, BindingResult bindingResult, Model model) {
        String name = school.getName();
        Optional<School> existSchool = schoolService.findByName(name);
        if (existSchool.isPresent()) {
            bindingResult.rejectValue("name", "error.school",
                    "Школа з такою назвою вже існує");
        }
        if (bindingResult.hasErrors()) {
            model.addAttribute("school", new School());
            model.addAttribute("teacher", new Teacher());
            model.addAttribute("schools",schoolService.findAll());
            return "admin";
        } else {
            schoolService.save(school);
            model.addAttribute("successMessage", "Школа успішно створенна!");
            model.addAttribute("school", new School());
            model.addAttribute("teacher", new Teacher());
            model.addAttribute("schools",schoolService.findAll());
        }
        return "admin";
    }

    @RequestMapping(value = "/teacher")
    public String createNewTeacher(@RequestParam String firstName,@RequestParam String lastName, @RequestParam String school,
                                   @RequestParam String subject,@RequestParam String description, Model model) {
        Teacher teacher = new Teacher();
        teacher.setFirstName(firstName);
        teacher.setLastName(lastName);
        teacher.setSchool(schoolService.findByName(school).get());
        teacher.setSubject(subject);
        teacher.setDescription(description);
        teacherService.save(teacher);
        model.addAttribute("successMessage", "Вчитель успішно створенний!");
        model.addAttribute("school", new School());
        model.addAttribute("teacher", new Teacher());
        model.addAttribute("schools",schoolService.findAll());
        return "admin";
    }



}
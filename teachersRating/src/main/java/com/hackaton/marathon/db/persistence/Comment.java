package com.hackaton.marathon.db.persistence;

import org.springframework.data.domain.Persistable;

import javax.persistence.*;
import java.util.Arrays;
import java.util.List;

@Entity
@Table(name = "comments")
public class Comment implements Persistable<Integer> {
    private Integer id;
    private String text;
    private Student student;
    private Teacher teacher;
    private int rate;
    private int difficult ;
    private char mark;
    private boolean wouldTake;
    private List<String> tags;

    @Id
    @Override
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getId() {
        return id;
    }

    @Override
    @Transient
    public boolean isNew() {
        return null == getId();
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "studentId")
    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "teacherId")
    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

    public int getDifficult() {
        return difficult;
    }

    public void setDifficult(int difficult) {
        this.difficult = difficult;
    }

    public char getMark() {
        return mark;
    }

    public void setMark(char mark) {
        this.mark = mark;
    }

    @ElementCollection
    @CollectionTable(name = "tags", joinColumns = @JoinColumn(name = "commentsId"))
    @Column(name = "tags")
    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public boolean isWouldTake() {
        return wouldTake;
    }

    public void setWouldTake(boolean wouldTake) {
        this.wouldTake = wouldTake;
    }
}

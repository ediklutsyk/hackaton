package com.hackaton.marathon.repositories;

import com.hackaton.marathon.db.persistence.Student;
import com.hackaton.marathon.db.persistence.Teacher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.w3c.dom.ls.LSInput;

import java.util.List;
import java.util.Optional;

@Repository
public interface TeacherRepository extends JpaRepository<Teacher, Integer> {
   List<Teacher> findAllBySubject(String subject);

   Optional<Teacher> findById(Integer id);
}

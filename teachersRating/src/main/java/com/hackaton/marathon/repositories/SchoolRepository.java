package com.hackaton.marathon.repositories;

import com.hackaton.marathon.db.persistence.School;
import com.hackaton.marathon.db.persistence.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SchoolRepository extends JpaRepository<School, Integer> {
    Optional<School> findByName(String name);
}

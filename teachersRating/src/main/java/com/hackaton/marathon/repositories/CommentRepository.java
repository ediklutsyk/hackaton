package com.hackaton.marathon.repositories;

import com.hackaton.marathon.db.persistence.Comment;
import com.hackaton.marathon.db.persistence.Teacher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Integer> {

}

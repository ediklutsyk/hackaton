package com.hackaton.marathon.services;

import com.hackaton.marathon.db.persistence.Student;

import java.util.List;
import java.util.Optional;

public interface StudentService {
    List<Student> findAll();

    Optional<Student> findBy(String email);

    Student save(Student student);

    Student update(Student student);

    void delete(Student student);
}

package com.hackaton.marathon.services;

import com.hackaton.marathon.db.persistence.Comment;
import com.hackaton.marathon.db.persistence.Student;
import com.hackaton.marathon.db.persistence.Teacher;
import com.hackaton.marathon.repositories.TeacherRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TeacherServiceImp implements TeacherService {
    private final TeacherRepository teacherRepository;
    private final StudentService studentService;
    private final CommentService commentService;

    public TeacherServiceImp(TeacherRepository teacherRepository, StudentService studentService, CommentService commentService) {
        this.teacherRepository = teacherRepository;
        this.studentService = studentService;
        this.commentService = commentService;
    }

    @Override
    public List<Teacher> findAll() {
        return teacherRepository.findAll();
    }

    @Override
    public List<Teacher> findAllBySubject(String subject) {
        return teacherRepository.findAllBySubject(subject);
    }

    @Override
    public Optional<Teacher> findById(Integer id){
        return teacherRepository.findById(id);
    }

    @Override
    public void addComment(Comment comment, Teacher teacher, Student student) {
        comment.setTeacher(teacher);
        comment.setStudent(student);
        Comment commentNew = commentService.save(comment);
        teacher.getComments().add(commentNew);
        student.getComments().add(commentNew);
        studentService.update(student);
        save(teacher);
    }

    @Override
    public Teacher save(Teacher teacher) {
        return teacherRepository.save(teacher);
    }

    @Override
    public void delete(Teacher teacher) {
        teacherRepository.delete(teacher);
    }
}

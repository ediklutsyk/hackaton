package com.hackaton.marathon.services;

import com.hackaton.marathon.db.persistence.Student;
import com.hackaton.marathon.repositories.StudentRepository;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class StudentServiceImp implements StudentService {
    private final StudentRepository studentRepository;
    private final ShaPasswordEncoder shaPasswordEncoder;

    public StudentServiceImp(StudentRepository studentRepository, ShaPasswordEncoder shaPasswordEncoder) {
        this.studentRepository = studentRepository;
        this.shaPasswordEncoder = shaPasswordEncoder;
    }

    @Override
    public List<Student> findAll() {
        return studentRepository.findAll();
    }

    @Override
    public Optional<Student> findBy(String email) {
        return studentRepository.findByEmail(email);
    }

    @Override
    public Student save(Student student) {
        student.setPassword(shaPasswordEncoder.encodePassword(student.getPassword(), null));
        student.setActive(1);
        return studentRepository.save(student);
    }

    @Override
    public Student update(Student student) {
        return studentRepository.save(student);
    }


    @Override
    public void delete(Student student) {
        studentRepository.delete(student);
    }
}

package com.hackaton.marathon.services;



import com.hackaton.marathon.db.persistence.Comment;
import com.hackaton.marathon.db.persistence.School;
import com.hackaton.marathon.db.persistence.Student;
import com.hackaton.marathon.db.persistence.Teacher;

import java.util.List;
import java.util.Optional;

public interface SchoolService {
    List<School> findAll();

    Optional<School> findByName(String name);

    School save(School school);

    void delete(School school);
}

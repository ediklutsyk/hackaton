package com.hackaton.marathon.services;



import com.hackaton.marathon.db.persistence.Comment;
import com.hackaton.marathon.db.persistence.Teacher;

import java.util.List;


public interface CommentService {
    List<Teacher> findAll();

    Comment save(Comment comment);

    void delete(Teacher teacher);
}

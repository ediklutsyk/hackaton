package com.hackaton.marathon.services;

import com.hackaton.marathon.db.persistence.Teacher;
import com.hackaton.marathon.db.persistence.*;
import com.hackaton.marathon.repositories.CommentRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommentServiceImp implements CommentService {
    private final CommentRepository commentRepository;

    public CommentServiceImp(CommentRepository commentRepository) {
        this.commentRepository = commentRepository;
    }

    @Override
    public List<Teacher> findAll() {
        return null;
    }

    @Override
    public Comment save(Comment comment) {
        return commentRepository.save(comment);
    }

    @Override
    public void delete(Teacher teacher) {

    }
}

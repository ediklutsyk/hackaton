package com.hackaton.marathon.services;

import com.hackaton.marathon.db.persistence.Comment;
import com.hackaton.marathon.db.persistence.Student;
import com.hackaton.marathon.db.persistence.Teacher;

import java.util.List;
import java.util.Optional;

public interface TeacherService {
    List<Teacher> findAll();

    List<Teacher> findAllBySubject (String subject);

    Optional<Teacher> findById(Integer id);

    void addComment(Comment comment, Teacher teacher, Student student);

    Teacher save(Teacher teacher);

    void delete(Teacher teacher);
}

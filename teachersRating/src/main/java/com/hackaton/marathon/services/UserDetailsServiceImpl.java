package com.hackaton.marathon.services;

import com.hackaton.marathon.db.persistence.Student;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Optional;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    private final StudentService studentService;

    public UserDetailsServiceImpl(StudentService studentService) {
        this.studentService = studentService;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        final Optional<Student> optionalStudent = studentService.findBy(username);
        if (optionalStudent.isPresent()) {
            final Student student = optionalStudent.get();
            return new org.springframework.security.core.userdetails.User(
                    student.getEmail(), student.getPassword(), Collections.singletonList(new SimpleGrantedAuthority(student.getRole()))
            );
        } else {
            throw new UsernameNotFoundException("Incorrect login or password.");
        }
    }
}
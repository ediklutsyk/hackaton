package com.hackaton.marathon.services;

import com.hackaton.marathon.db.persistence.School;
import com.hackaton.marathon.db.persistence.Student;
import com.hackaton.marathon.repositories.SchoolRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class SchoolServiceImp implements SchoolService {
    private final SchoolRepository schoolRepository;

    public SchoolServiceImp(SchoolRepository schoolRepository) {
        this.schoolRepository = schoolRepository;
    }

    @Override
    public List<School> findAll() {
        return schoolRepository.findAll();
    }

    @Override
    public Optional<School> findByName(String name) {
        return schoolRepository.findByName(name);
    }

    @Override
    public School save(School school) {
        return schoolRepository.save(school);
    }

    @Override
    public void delete(School school) {
        schoolRepository.delete(school);
    }
}
